package com.narsicloud.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.narsicloud.*")
public class NCAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(NCAuthApplication.class, args);
	}
}
