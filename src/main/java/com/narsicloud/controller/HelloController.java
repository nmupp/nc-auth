package com.narsicloud.controller;

import java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@GetMapping(path="/welcome")
	public Message name() {
		return new Message("Hello World");
	}
	
	@GetMapping(path="/user")
	public Principal principal(Principal principal) {
		return principal;
	}

}
